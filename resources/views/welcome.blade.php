<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AZDrive</title>
        <link rel="icon" href="{!! asset('images/azdrive_logo.png') !!}"/>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 90vh;
                margin: 0;
            }

            .full-height {
                height: 90vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <script src="{{ asset('jquery.js') }}"></script>
        <script src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>
    </head>
    <body>
    
        <div class="flex-center position-ref full-height">
           <div class="content">
                <div class="title m-b-md">
                    AZ Drive
                </div>
                <div class="m-b-md">
                <h3>
                    www.azdrive.com.ar
                </h3>
                </div>
                <div>
                    <a href="http://www.azdrive.com.ar:8080/RGPS_WF2">
                        <img src= "images/azdrive_logo.png"  />
                    </a>
                </div>
            </div>
        </div>
    </body>
    <footer class="col-md-10 offset-md-5">

            <HR/>
                <div class="flex-center position-ref">
                    <div>   +54-9-11-5475-8673
                        <a href="mailto:info.azdrive@az-energy.com?Subject=Solicitud%20de%20información">info.azdrive@az-energy.com</a>
                        España 1038, Ayacucho, Buenos Aires- Argentina
                    </div>
                </div>
    </footer>
</html>
